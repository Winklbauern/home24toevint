package tcon.assignment.groovy

import tcon.util.Message

public class AssignmentsToEvint {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public AssignmentsToEvint(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		this.processData(message);
	}


	Message processData(Message message) {

		//Url for Evint-API
		def url = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/";

		//***********************************************************************************
		// TRACER Declaration
		//***********************************************************************************
		def tracer = true;
		def sTracer = "";

		//***********************************************************************************
		//get SF Employees
		//***********************************************************************************
		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);

		//catch them all
		try {

			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();
			//sfEmployees = jsonSlurper.parseText(body).queryCompoundEmployeeResponse.CompoundEmployee;

			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			sfEmployees = jsonSlurper.parseText(message.getBody("AllSfEmployees"));
			//**CLOUD SETTINGS*******************************************************************

			//***********************************************************************************
			//get Evint Details
			//***********************************************************************************

			def evintEmployees            = this.getList(url + "employees", messageLog, jsonSlurper);
			def evintAbscenceGroups       = this.getList(url + "absenceGroups", messageLog, jsonSlurper);
			def evintCostis               = this.getList(url + "costCenters", messageLog, jsonSlurper);
			def evintPayrollDefinitions   = this.getList(url + "payrollDefinitions", messageLog, jsonSlurper);
			def evintTimeAccountGroups    = this.getList(url + "timeAccountGroups", messageLog, jsonSlurper);
			def evintWage                 = this.getList(url + "wageTypeGroups", messageLog, jsonSlurper);
			def evintWorktime             = this.getList(url + "worktimeDefinitions", messageLog, jsonSlurper);

			def evintAbscenceAssignments = this.getList(url + "employeeAssignmentsAbsenceGroup?include=employees,absenceGroups", messageLog, jsonSlurper);
			def evintCostCenterAssignments = this.getList(url + "employeeAssignmentsCostCenter?include=employees,costCenters", messageLog, jsonSlurper);
			def evintPayrollDefinitionAssignments = this.getList(url + "employeeAssignmentsPayrollDefinition?include=employees,payrollDefinitions", messageLog, jsonSlurper);
			def evintTimeAccountGroupAssignments = this.getList(url + "employeeAssignmentsTimeAccountGroup?include=employees,timeAccountGroup", messageLog, jsonSlurper);
			def evintWageAssignments = this.getList(url + "employeeAssignmentsWageTypeGroup?include=employees,wageTypeGroups", messageLog, jsonSlurper);
			def evintWorkTimeAssignments = this.getList(url + "employeeAssignmentsWorktimeDefinition?include=employees,worktimeDefintions", messageLog, jsonSlurper);


			//***********************************************************************************
			// processing of the data
			//***********************************************************************************
			for (i = 0; i < sfEmployees.size(); i++) {
				//for (i = 0; i < 50; i++) {
				def sfEmployee = sfEmployees.get(i).person;

				//only for active Users
				if (! (sfEmployee.logon_user_is_active.toBoolean())) {
					continue;
				}

				//do only for this person
				//if (! (sfEmployee.person_id_external.equals("4525") || sfEmployee.person_id_external.equals("4300") || sfEmployee.person_id_external.equals("2408"))) {
				//  continue;
				//}

				if (tracer) {
					sTracer = sTracer + "\nPersonID:" + sfEmployee.person_id_external
				};
				def personalNumber = null;
				def evintEmpId = null;
				try {
					personalNumber = sfEmployee.person_id_external;
					def personalInformation = this.getCorrectObject(sfEmployee.personal_information);
					def employmentInformation = this.getCorrectObject(sfEmployee.employment_information);
					def jobInfo = this.getCorrectObject(employmentInformation.job_information);



					for (def j = 0; j < evintEmployees.size(); j++) {
						if (evintEmployees[j]["attributes"]["personnelNumber"].equals(personalNumber)) {
							evintEmpId = evintEmployees[j]["id"];
						}
					}
					if (!evintEmpId) {
						//if location = GWS --> candidate actually not inserted in Evint
						if (! (jobInfo && jobInfo.location && jobInfo.location.equals("GWS"))) {
							if (tracer) {
								sTracer = sTracer + "\tEmployee not Exist in Evint";
							}
							continue;
						} else if(jobInfo.location.equals("GWS") ){
							if (tracer) {
								sTracer = sTracer + "\tCandidate has Location GWS";
							}
							continue;
						}
					}
					//***********************************************************************************
					// Abscence Groups assignMent
					//***********************************************************************************
					def absGroupId = null;
					if (jobInfo && jobInfo.location) {
						absGroupId = this.getAbsGroupId(jobInfo.location);
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, AbscenceGroups wurde in SuccessFactors nicht gefüllt."
						};
					}

					if (absGroupId) {
						def validValue = this.getValidValue(evintAbscenceAssignments, "absenceGroups", evintEmpId, absGroupId);
						def from = this.getFrom(evintAbscenceGroups,jobInfo.start_date,absGroupId);
						if ((!validValue) && evintEmpId && jobInfo) {
							if (tracer) {
								sTracer = sTracer + "POST: absGroupId:" + absGroupId;
							};
							this.doAssignMentPost("absenceGroups", absGroupId, evintEmpId, from, "employeeAssignmentsAbsenceGroup", "employeeAssignmentsAbsenceGroup", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tAbsenceGroupId:" + absGroupId + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}


					//***********************************************************************************
					// cost Center assignMent
					//***********************************************************************************
					def costId = null;
					if (jobInfo && jobInfo.cost_center) {
						costId = this.getItemOfList(evintCostis, jobInfo.cost_center);
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, Cost Center wurde in SuccessFactors nicht gefüllt."
						};
					}

					if (costId) {
						def validValue = this.getValidValue(evintCostCenterAssignments, "costCenters", evintEmpId, costId);
						def from = this.getFrom(evintCostis,jobInfo.start_date,costId);
						if ((!validValue) && evintEmpId && jobInfo) {
							this.doAssignMentPost("costCenters", costId, evintEmpId, from, "employeeAssignmentsCostCenter", "employeeAssignmentsCostCenter", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tCostId:" + costId + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}

					//***********************************************************************************
					// payrollDef
					//***********************************************************************************
					def payrollDef = null;
					if (jobInfo && jobInfo.custom_string5) {
						payrollDef = this.getPayId(jobInfo.custom_string5);
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, Payroll wurde in SuccessFactors nicht gefüllt."
						};
					}

					if (payrollDef) {
						def validValue = this.getValidValue(evintPayrollDefinitionAssignments, "payrollDefinitions", evintEmpId, payrollDef);
						def from = this.getFrom(evintPayrollDefinitions,jobInfo.start_date,payrollDef);
						if ((!validValue) && evintEmpId && jobInfo) {
							this.doAssignMentPost("payrollDefinitions", payrollDef, evintEmpId, from, "employeeAssignmentsPayrollDefinition", "employeeAssignmentsPayrollDefinition", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tPayRollId:" + payrollDef + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}

					//***********************************************************************************
					// Time Account assignMent
					//***********************************************************************************
					def azkId = null;
					if (jobInfo && jobInfo.custom_string6) {
						azkId = this.getAzkId(jobInfo.custom_string6);
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, AZK-Gruppe wurde in SuccessFactors nicht gefüllt."
						};
					}

					if (azkId) {
						def validValue = this.getValidValue(evintTimeAccountGroupAssignments, "timeAccountGroup", evintEmpId, azkId);
						def from = this.getFrom(evintTimeAccountGroups,jobInfo.start_date,azkId);
						if ((!validValue) && evintEmpId && jobInfo) {
							this.doAssignMentPost("timeAccountGroup", azkId, evintEmpId, from, "employeeAssignmentsTimeAccountGroup", "employeeAssignmentsTimeAccountGroup", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tAZKId:" + azkId + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}

					//***********************************************************************************
					// wageTypeAssignment
					//***********************************************************************************
					def wageId = null;
					if (jobInfo && jobInfo.custom_string7) {
						wageId = this.getItemOfList(evintWage, jobInfo.custom_string7.replace("Hourly Staff", "Stundenlohnempfänger").replace("-", " - ").replace("Salaried Employee", "Gehaltsempfänger"));
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, WageType wurde in SuccessFactors nicht gefüllt."
						};
					}
					if (wageId) {
						def validValue = this.getValidValue(evintWageAssignments, "wageTypeGroups", evintEmpId, wageId);
						def from = this.getFrom(evintWage,jobInfo.start_date,wageId);
						if ((!validValue) && evintEmpId && jobInfo) {
							this.doAssignMentPost("wageTypeGroups", wageId, evintEmpId, from, "employeeAssignmentsWageTypeGroup", "employeeAssignmentsWageTypeGroup", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "Es konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tWageId:" + wageId + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}

					//***********************************************************************************
					// work Time assignMent
					//***********************************************************************************
					def workid = null;
					if (jobInfo && jobInfo.standard_hours && jobInfo.location && jobInfo.workschedule_code) {
						workid = this.getWorkId(jobInfo.standard_hours, jobInfo.location,jobInfo.workschedule_code);
					} else {
						if (tracer) {
							sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, WorktimeDefinition wurde in SuccessFactors nicht gefüllt."
						};
					}

					if (workid) {
						def validValue = this.getValidValue(evintWorkTimeAssignments, "worktimeDefintions", evintEmpId, workid);
						def from = this.getFrom(evintWorktime,jobInfo.start_date,workid);
						if ((!validValue) && evintEmpId && jobInfo) {
							this.doAssignMentPost("worktimeDefintions", workid, evintEmpId, from, "employeeAssignmentsWorktimeDefinition", "employeeAssignmentWorktimeDefinition", messageLog);
						} else {
							if ((!validValue)) {
								if (tracer) {
									sTracer = sTracer + "\tEs konnte kein POST durchgeführt werden, eines der folgenden Felder konnte nicht ermittelt werden!\tWorkTimeId:" + workid + "\tEvintEmployeeId:" + evintEmpId + "\tJobInfo.start_date" + JsonOutput.toJson(jobInfo)
								};
							}
						}
					}

				} catch(Exception ex) {
					def exceptionOutput = "Exceptionmessage:\n" + ex.toString() + "\n\nStackTrace:";
					def aStack = ex.getStackTrace();
					for(def j = 0;j<aStack.size();j++) {
						def oStack = aStack[j];
						if(oStack.toString().contains("tcon")) {
							exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
						}
					}
					if (sfEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nSFEmployee:\n" + JsonOutput.toJson(sfEmployee);
					}
					if (evintEmpId != null) {
						exceptionOutput = exceptionOutput + "\n\nEvintEmployee:\n" + JsonOutput.toJson(evintEmpId);
					}
					if(tracer){sTracer = sTracer + "\nERROR für "+personalNumber+" :"+exceptionOutput};
				}

			}
		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}
		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};


		return message;
	}

	public Object getValidValue(evintAssignments, String type, String evintEmpId, String sfTypeId) {
		for (def j = 0; j < evintAssignments.size(); j++) {
			def assignMent = evintAssignments[j];
			def empId = this.getRelationshipId(assignMent, "employees");
			def typeId = this.getRelationshipId(assignMent, type);

			def startEvint = new Date().parse("yyyy-MM-dd", assignMent["attributes"]["from"]);
			def now = new Date();
			def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());

			def endEvint = null;
			if (assignMent["attributes"]["to"]) {
				endEvint = new Date().parse("yyyy-MM-dd", assignMent["attributes"]["to"]);
			}
			if (empId.equals(evintEmpId)) {
				//end null && start before actual Date
				if ((!endEvint) && startEvint.getTime() <= dayNow.getTime() && typeId.equals(sfTypeId)) {
					return assignMent;
				} else if (startEvint.getTime() <= dayNow.getTime() && endEvint && endEvint.getTime() >= dayNow.getTime() && typeId.equals(sfTypeId)) {
					return assignMent;
				}
			}
		}
		return null;
	}

	public Object getFrom(evintAssignments, String jobInfoFrom, String sfTypeId) {
		for (def j = 0; j < evintAssignments.size(); j++) {
			def assignMent = evintAssignments[j];
			!(null )
			if(!(assignMent.id.equals(sfTypeId)) || !(assignMent.attributes.validTo == null)){
				continue;
			}

			def fromEvint = null;
			//if from not filled
			if (assignMent.attributes.validFrom) {
				fromEvint = new Date().parse("yyyy-MM-dd", assignMent.attributes.validFrom.substring(0,10));
			}
			def jobFrom = new Date().parse("yyyy-MM-dd", jobInfoFrom);
			if(fromEvint){
				if (fromEvint.getTime() <= jobFrom.getTime()) {
					return jobInfoFrom;
				} else{
					return assignMent.attributes.validFrom.substring(0,10);
				}
			}else{
				return jobInfoFrom;
			}
		}
		return null;
	}

	public void doAssignMentPost(String type, String assignId, String emplId, String from, String url, String urlType, messageLog) {
		def evint = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/";
		//POST
		def requestMap = [: ],
		dataMap = [: ],
		attributesMap = [: ],
		relationshipsMap = [: ],
		employeesMap = [: ],
		employeesDataMap = [: ],
		assignMap = [: ],
		assignDataMap = [: ];

		//wageTypeGroup
		assignDataMap.put("type", type);
		assignDataMap.put("id", assignId);
		assignMap.put("data", assignDataMap);

		//employees
		employeesDataMap.put("type", "employees");
		employeesDataMap.put("id", emplId);
		employeesMap.put("data", employeesDataMap);

		relationshipsMap.put("employees", employeesMap);
		relationshipsMap.put(type, assignMap);

		attributesMap.put("from", from);

		dataMap.put("type", urlType);
		dataMap.put("attributes", attributesMap);
		dataMap.put("relationships", relationshipsMap);

		requestMap.put("data", dataMap);
		this.httpPatch(evint + url, JsonOutput.toJson(requestMap), messageLog, "POST");
	}

	public String getItemOfList(Object list, String searchFor) {
		if (!list) {
			return null;
		}
		for (def i = 0; i < list.size(); i++) {
			if (list[i].attributes.name.contains(searchFor)) {
				return list[i].id;
			}
		}
		return null;
	}

	public String httpGet(String urlString, messageLog) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpGet(urlString, messageLog);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
			}
		}
	}

	public String httpPatch(String urlString, String json, messageLog, String type) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		if (type.equals("PATCH")) {
			urlConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
		}
		urlConnection.setRequestMethod("POST");
		urlConnection.setDoOutput(true);
		urlConnection.getOutputStream().write(json.getBytes("UTF-8"));
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpPatch(urlString, json, messageLog, type);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\n" + type + " " + urlString + "\n" + json);
			}
			
		}

	}

	public Object getList(String request, messageLog, jsonSlurper) {
		def evintFirstFifty = jsonSlurper.parseText(this.httpGet(request, messageLog)).data;
		def evintList = evintFirstFifty;
		def urlAppend = "?page[offset]=";
		if (request.contains("?")) {
			urlAppend = "&page[offset]=";
		}

		//Evint API only return 50 Entry
		def counter = 50;
		while (evintFirstFifty.size() == 50) {
			evintFirstFifty = jsonSlurper.parseText(this.httpGet(request + urlAppend + counter, messageLog)).data;
			evintList.addAll(evintFirstFifty);
			counter = counter + 50;
		}
		return evintList;
	}

	public String getAzkId(String azk) {
		if (!azk) {
			return null;
		}
		return "048b4c67-8531-4e6f-8fc9-321476457346";
	}

	public String getAbsGroupId(String absSf) {
		if (!absSf) {
			return null;
		}
		if (absSf.equals("HS")) {
			return "5b68f629-2f73-4af1-b8c7-51cecf104dfb";
		} else if (absSf.equals("LF")) {
			return "fe242cbd-1dcd-414b-9d3b-7cd81807ee9a";
		} else if (absSf.equals("WR")) {
			return "c0926cfd-cbdf-44f0-a801-b7ba79b1daf1";
		} else {
			return null;
		}
	}

	public String getPayId(String payId) {
		if (!payId) {
			return null;
		}
		if(payId.equals("HS_16-15_mon")) {
			return "49b9d786-d6ed-4ae4-9937-42270d989f59";
		}
		else if(payId.equals("HS_mon")) {
			return "8c32edcb-e0fd-485c-ae96-ccc2c1642130";
		}
		else if(payId.equals("HS_mon_TWT")) {
			return "4aa28727-4726-47d4-9c04-29aed01b33ac";
		}
		else if(payId.equals("LF_mon(new)")) {
			return "90550cc7-8b5e-41db-a954-44849e68e2ef";
		}
		else if(payId.equals("LF_TWT")) {
			return "5b460faa-8ccd-4aa5-8095-f48687829e03";
		}
		else if(payId.equals("WR_16-15_mon")) {
			return "28135f52-684b-4fc2-9ee4-de975d71281c";
		}
		else if(payId.equals("WR_mon")) {
			return "c76cbcc7-dff4-429f-9ae2-47b3efcc9cd1";
		}
		else if(payId.equals("WR_mon_incl10")) {
			return "4081941f-7f05-4a3a-89e6-892ec64da680";
		}
		else if(payId.equals("WR_mon_incl15")) {
			return "a2fd49cc-1a97-44f9-998d-ce00220e6a17";
		}
		else if(payId.equals("WR_mon_incl20")) {
			return "a3f62e66-c8d8-48b8-a003-d0e65ccb6f4a";
		}
		else if(payId.equals("WR_mon_incl5")) {
			return "3b593b0f-1044-4e2e-b60f-8943234a93df";
		}
		else if(payId.equals("WR_mon_TWT")) {
			return "f800c5ca-2955-4ddb-8388-962b579aae70";
		}


	}

	public String getWorkId(String hours, String location, String workSchedule) {
		// convert 40.0 --> 40
		double time = hours as Double;
		int timeInt = time;

		def workSf = location + " " + timeInt;
		if (workSf.equals("HS 20")) {
			return "4a9cb661-10ad-4ae9-a01f-f1e51c188126";
		} else if (workSf.equals("HS 32")) {
			return "aebb17da-40a8-4cf1-9191-e755f7b26739";
		} else if (workSf.equals("HS 40")) {
			return "76ade661-7d10-4850-a7a8-ab955f9e7dd3";
		} else if (workSf.equals("LF 30")) {
			return "b3afa71c-e5c8-48e1-9195-35fb73655a9e";
		} else if (workSf.equals("LF 32")) {
			return "1247cfa9-5b62-478e-9d2a-b62db5454ce8";
		} else if (workSf.equals("LF 35")) {
			return "8062e16e-49ab-47d9-8127-3519cef86883";
		} else if (workSf.equals("LF 40")) {
			return "64c5dc66-b6f2-4a8e-8986-db9efadbf9d2";
		} else if (workSf.equals("WR 30") && workSchedule.equals("D5_5")) {
			return "42ff309d-ebb3-4109-9de9-317b1f0f3970";
		} else if (workSf.equals("WR 30") && workSchedule.equals("D4_1")) {
			return "363a886c-d552-4951-8fb9-9d3cdf400259";
		} else if (workSf.equals("WR 40")) {
			return "57d837bb-0f1c-4ba8-ae50-620f16515a2c";
		} else {
			return null;
		}

	}

	public String getRelationshipId(Object object, String identifier) {
		return object["relationships"][identifier]["data"]["id"];
	}

	public Object getCorrectObject(Object object) {
		if (object instanceof java.util.ArrayList) {
			for (def i = 0; i < object.size(); i++) {
				def oEntity = object[i];
				def start = null;
				try{
					start = new Date().parse("yyyy-MM-dd", oEntity.start_date);
				}catch(Exception e){
					throw new Exception("Could not parse: "+oEntity);
				}
				def end = null;
				//if enddate not filled --> 9999-12-12
				if (oEntity.end_date) {
					end = new Date().parse("yyyy-MM-dd", oEntity.end_date);
				} else {
					end = new Date().parse("yyyy-MM-dd", "9999-12-31");
				}
				def now = new Date();
				def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());
				if (start.getTime() <= dayNow.getTime() && end.getTime() >= dayNow.getTime()) {
					return oEntity;
				}
			}
		} else {
			return object;
		}

	}
}