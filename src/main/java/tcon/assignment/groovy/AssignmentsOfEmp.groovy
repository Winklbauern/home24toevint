package tcon.assignment.groovy

import tcon.util.Message

public class AssignmentsOfEmp {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public AssignmentsOfEmp(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		this.processData(message);
	}



	Message processData(Message message) {

		//Url for Evint-API
		def url = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/";

		//***********************************************************************************
		// TRACER Declaration
		//***********************************************************************************
		def tracer = true;
		def sTracer = "";

		//***********************************************************************************
		//get SF Employees
		//***********************************************************************************
		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);

		//catch them all
		try {

			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();
			//sfEmployees = jsonSlurper.parseText(body).queryCompoundEmployeeResponse.CompoundEmployee;

			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			sfEmployees = jsonSlurper.parseText(message.getBody("AllSfEmployees"));
			//**CLOUD SETTINGS*******************************************************************


			//***********************************************************************************
			//get Evint Details
			//***********************************************************************************

			def evintEmployees            = this.getList(url + "employees", messageLog, jsonSlurper);
			def evintAbscenceGroups       = this.getList(url + "absenceGroups", messageLog, jsonSlurper);
			def evintCostis               = this.getList(url + "costCenters", messageLog, jsonSlurper);
			def evintPayrollDefinitions   = this.getList(url + "payrollDefinitions", messageLog, jsonSlurper);
			def evintTimeAccountGroups    = this.getList(url + "timeAccountGroups", messageLog, jsonSlurper);
			def evintWage                 = this.getList(url + "wageTypeGroups", messageLog, jsonSlurper);
			def evintWorktime             = this.getList(url + "worktimeDefinitions", messageLog, jsonSlurper);

			def evintAbscenceAssignments = this.getList(url + "employeeAssignmentsAbsenceGroup?include=employees,absenceGroups", messageLog, jsonSlurper);
			def evintCostCenterAssignments = this.getList(url + "employeeAssignmentsCostCenter?include=employees,costCenters", messageLog, jsonSlurper);
			def evintPayrollDefinitionAssignments = this.getList(url + "employeeAssignmentsPayrollDefinition?include=employees,payrollDefinitions", messageLog, jsonSlurper);
			def evintTimeAccountGroupAssignments = this.getList(url + "employeeAssignmentsTimeAccountGroup?include=employees,timeAccountGroup", messageLog, jsonSlurper);
			def evintWageAssignments = this.getList(url + "employeeAssignmentsWageTypeGroup?include=employees,wageTypeGroups", messageLog, jsonSlurper);
			def evintWorkTimeAssignments = this.getList(url + "employeeAssignmentsWorktimeDefinition?include=employees,worktimeDefintions", messageLog, jsonSlurper);

			//***********************************************************************************
			// write SF and Evint JSONS in File for Integration Flow OUTPUT Files for showing
			// if trace is ebabled
			//***********************************************************************************
			if (tracer) {
				//Employee
				//messageLog.addAttachmentAsString("AllSfEmployees", JsonOutput.toJson(sfEmployees), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintEmployees", JsonOutput.toJson(evintEmployees), "text/plain");

				//Accounting
				//messageLog.addAttachmentAsString("AllEvintAbsenceGroups", JsonOutput.toJson(evintAbscenceGroups), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintCostCenter", JsonOutput.toJson(evintCostis), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintPayRolls", JsonOutput.toJson(evintPayrollDefinitions), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintTimeAccounts", JsonOutput.toJson(evintTimeAccountGroups), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintWageType", JsonOutput.toJson(evintWage), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintWorktime", JsonOutput.toJson(evintWorktime), "text/plain");

				//Assignments
				//messageLog.addAttachmentAsString("AllEvintAbscenceAssignments", JsonOutput.toJson(evintAbscenceAssignments), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintCostCenterAssignments", JsonOutput.toJson(evintCostCenterAssignments), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintPayrollDefinitionAssignments", JsonOutput.toJson(evintPayrollDefinitionAssignments), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintTimeAccountGroupAssignments", JsonOutput.toJson(evintTimeAccountGroupAssignments), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintWageAssignments", JsonOutput.toJson(evintWageAssignments), "text/plain");
				//messageLog.addAttachmentAsString("AllEvintWorkTimeAssignments", JsonOutput.toJson(evintWorkTimeAssignments), "text/plain");
			}
			//***********************************************************************************
			// processing of the data
			//***********************************************************************************
			for (def i = 0; i < sfEmployees.size(); i++) {
				//for (i = 0; i < 50; i++) {
				def sfEmployee = sfEmployees.get(i).person;

				//only for active Users
				if (! (sfEmployee.logon_user_is_active.toBoolean())) {
					continue;
				}

				//do only for this person
				//if (! (sfEmployee.person_id_external.equals("4525") || sfEmployee.person_id_external.equals("4300") || sfEmployee.person_id_external.equals("2408"))) {
				//  continue;
				//}
				//if (! (sfEmployee.person_id_external.equals("5041"))) {
				//  continue;
				//}
				if (tracer) {
					sTracer = sTracer + "\nPersonID:" + sfEmployee.person_id_external
				};
				def personalNumber = null;
				def evintEmpId = null;
				try {
					personalNumber = sfEmployee.person_id_external;
					def personalInformation = this.getCorrectObject(sfEmployee.personal_information);
					def employmentInformation = this.getCorrectObject(sfEmployee.employment_information);
					def jobInfo = this.getCorrectObject(employmentInformation.job_information);



					for (def j = 0; j < evintEmployees.size(); j++) {
						if (evintEmployees[j]["attributes"]["personnelNumber"].equals(personalNumber)) {
							evintEmpId = evintEmployees[j]["id"];
						}
					}
					if (!evintEmpId) {
						//if location = GWS --> candidate actually not inserted in Evint
						if (! (jobInfo && jobInfo.location && jobInfo.location.equals("GWS"))) {
							throw new Exception("Employee not Exist in Evint");
						}

					}
					if (tracer) {
						sTracer = sTracer + this.getAssignments([evintAbscenceAssignments, evintCostCenterAssignments, evintPayrollDefinitionAssignments, evintTimeAccountGroupAssignments, evintWageAssignments, evintWorkTimeAssignments],["absenceGroups", "costCenters", "payrollDefinitions", "timeAccountGroup", "wageTypeGroups", "worktimeDefintions"],[evintAbscenceGroups, evintCostis, evintPayrollDefinitions, evintTimeAccountGroups, evintWage, evintWorktime],evintEmpId);
					}

					//if (tracer) {
					//    sTracer = sTracer + this.getAssignments([evintAbscenceAssignments],["absenceGroups"],[evintAbscenceGroups],evintEmpId);
					//};




				} catch(Exception ex) {
					def exceptionOutput = "Exceptionmessage:\n" + ex.toString() + "\n\nStackTrace:";
					def aStack = ex.getStackTrace();
					for(def j = 0;j<aStack.size();j++) {
						def oStack = aStack[j];
						if(oStack.toString().contains("tcon")) {
							exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
						}
					}
					if (sfEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nSFEmployee:\n" + JsonOutput.toJson(sfEmployee);
					}
					if (evintEmpId != null) {
						exceptionOutput = exceptionOutput + "\n\nEvintEmployee:\n" + JsonOutput.toJson(evintEmpId);
					}
					if(tracer){sTracer = sTracer + "\nERROR für "+personalNumber+" :"+exceptionOutput};
				}

			}
		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}
		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};


		return message;
	}

	public String getAssignments(assignments, names, evintNames, empId){
		def returner = "\n\tEmpId:"+empId;
		for(def i = 0; i<assignments.size();i++){
			for(def j = 0; assignments[i].size(); j++){
				try{
					if(assignments[i][j] == null){
						break;
					}
					if(assignments[i][j]["relationships"]["employees"]["data"]["id"].equals(empId)){
						for(def k = 0; k<evintNames[i].size(); k++){
							if(evintNames[i][k]["id"].equals(assignments[i][j]["relationships"][names[i]]["data"]["id"])){
								returner = returner + "\n\t"+names[i]+":"+evintNames[i][k]["attributes"]["name"] +"\tfrom:"+assignments[i][j]["attributes"]["from"]+"\tto:"+assignments[i][j]["attributes"]["to"];
							}
						}
					}
				}catch(Exception e){
					throw new Exception("i:"+i+"\tj:"+j+"\tname:"+names[i]);
				}

			}
		}
		return returner;
	}





	public String httpGet(String urlString, messageLog) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpPatch(urlString, json, messageLog, type);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
			}
		}
	}

	public String httpPatch(String urlString, String json, messageLog, String type) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		if (type.equals("PATCH")) {
			urlConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
		}
		urlConnection.setRequestMethod("POST");
		urlConnection.setDoOutput(true);
		urlConnection.getOutputStream().write(json.getBytes("UTF-8"));
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpPatch(urlString, json, messageLog, type);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\n" + type + " " + urlString + "\n" + json);

			}
		}

	}

	public Object getList(String request, messageLog, jsonSlurper) {
		def evintFirstFifty = jsonSlurper.parseText(this.httpGet(request, messageLog)).data;
		def evintList = evintFirstFifty;
		def urlAppend = "?page[offset]=";
		if (request.contains("?")) {
			urlAppend = "&page[offset]=";
		}

		//Evint API only return 50 Entry
		def counter = 50;
		while (evintFirstFifty.size() == 50) {
			evintFirstFifty = jsonSlurper.parseText(this.httpGet(request + urlAppend + counter, messageLog)).data;
			evintList.addAll(evintFirstFifty);
			counter = counter + 50;
		}
		return evintList;
	}




	public Object getCorrectObject(Object object) {
		if (object instanceof java.util.ArrayList) {
			for (def i = 0; i < object.size(); i++) {
				def oEntity = object[i];
				def start = new Date().parse("yyyy-MM-dd", oEntity.start_date);
				def end = null;
				//if enddate not filled --> 9999-12-12
				if (oEntity.end_date) {
					end = new Date().parse("yyyy-MM-dd", oEntity.end_date);
				} else {
					end = new Date().parse("yyyy-MM-dd", "9999-12-31");
				}
				def now = new Date();
				def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());
				if (start.getTime() <= dayNow.getTime() && end.getTime() >= dayNow.getTime()) {
					return oEntity;
				}
			}
		} else {
			return object;
		}

	}
}