package tcon.util;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import groovy.json.JsonOutput;
import groovy.json.JsonSlurper;
import tcon.absence.absence.groovy.AbsenceToEvint;
import tcon.absence.quota.groovy.AbsenceQuotaToEvint;
import tcon.assignment.groovy.AssignmentsOfEmp;
import tcon.testarea.TestArea;

public class Main {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//JsonSlurper slurper = new JsonSlurper();
		
		Main.disableCertificateValidation();
		
		//AbsenceQuota
//		AbsenceQuotaToEvint oAbsenceQuota = new AbsenceQuotaToEvint(new JsonSlurper(),new JsonOutput(),new Message());
//		oAbsenceQuota.doAll();
		
		//Absence
//		AbsenceToEvint oAbsenceToEvint = new AbsenceToEvint(new JsonSlurper(),new JsonOutput(),new Message());
//		oAbsenceToEvint.doAll();
		
		//Assignments
//		AssignmentsToEvint oAssignmentsToEvint = new AssignmentsToEvint(new JsonSlurper(),new JsonOutput(),new Message());
//		oAssignmentsToEvint.doAll();
		
		//Employee
//		EmployeeToEvint oEmpToEvint = new EmployeeToEvint(new JsonSlurper(),new JsonOutput(),new Message());
//		oEmpToEvint.doAll();
		
		AssignmentsOfEmp oAssignOfEmp = new AssignmentsOfEmp(new JsonSlurper(),new JsonOutput(),new Message());
		oAssignOfEmp.doAll();
		//Testarea
//		TestArea oTestArea = new TestArea(new JsonSlurper(),new JsonOutput(),new Message());
//		oTestArea.doAll();
		
	}

    public static void disableCertificateValidation() throws Exception {
    	 // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] { 
          new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() { 
              return new X509Certificate[0]; 
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
        }};

        // Ignore differences between given hostname and certificate hostname
        HostnameVerifier hv = new HostnameVerifier() {
          public boolean verify(String hostname, SSLSession session) { return true; }
        };

        // Install the all-trusting trust manager
        try {
          SSLContext sc = SSLContext.getInstance("SSL");
          sc.init(null, trustAllCerts, new SecureRandom());
          HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
          HttpsURLConnection.setDefaultHostnameVerifier(hv);
        } catch (Exception e) {}
    }
	
}
