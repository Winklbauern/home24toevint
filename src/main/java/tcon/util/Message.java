package tcon.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Message {
	
	public String sBody = "";
	
	public String sPath = "C:\\Development\\Workspaces\\home24\\Home24ToEvint\\src\\main\\java\\tcon\\files\\";
	
	public String sFileType = ".txt";
	
	public Message() {
		//do absolutely nothing
	}
	
	public String getBody(String sFile) throws IOException {
        try (FileReader oFr = new FileReader(sPath+sFile+sFileType);
             BufferedReader oReader = new BufferedReader(oFr)) {
            String sReturner = "",sLine;
            while ((sLine = oReader.readLine()) != null) {
            	sReturner = sReturner + sLine;
            };
            return sReturner;
        } catch (IOException e) {
            throw e;
        }

	}
	public String getBody(Class<String> oClazz) {
		return sBody;
	}
	public Message getMessageLog(Message m) {
		return m;
	}
	public void addAttachmentAsString(String sFileName, String sBody, String sType) throws IOException {
		String sPath= this.sPath+sFileName+sFileType;
		File oFile = new File(sPath);
		if(!oFile.exists()) {
			oFile.createNewFile();
		}
		FileWriter oFw = new FileWriter(oFile);
		oFw.write(sBody);
		oFw.flush();
		oFw.close();

	}
	
}
