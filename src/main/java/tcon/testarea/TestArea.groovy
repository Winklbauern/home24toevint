package tcon.testarea

import tcon.util.Message


public class TestArea {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public TestArea(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		message.setExtension("testarea\\");
		processData(message);
	}





	Message processData(Message message) {

		//TRACER:
		def tracer = true;
		def sTracer = "";

		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);

		def sEvintUrl = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/";
		//catch them all
		try {

			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();

			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			//**CLOUD SETTINGS*******************************************************************

			def requestMap = [: ],
			expressionMap = [: ];
			expressionMap.put("attribute", "date");
			expressionMap.put("comparison", "gte");
			expressionMap.put("value", "2021-01-29");
			requestMap.put("operation", "and");
			requestMap.put("expressions", [expressionMap]);
			if(tracer){sTracer = sTracer + "\nJSON:"+JsonOutput.toJson(requestMap)};
			println JsonOutput.toJson(requestMap).bytes.encodeBase64().toString();
//			println Base64.decoder.decode(JsonOutput.toJson(requestMap));
			println Base64.encoder.encode(JsonOutput.toJson(requestMap));

			//def absenceEvint = this.getList(sEvintUrl + "absences?include=absenceTypes,employees", jsonSlurper);

		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}
		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};


		return message;
	}

	public Object getList(String request, jsonSlurper) {
		def firstFifty = null,
		urlAppend = null,
		counter = null,
		list = null;
		if(request.contains("evint")){
			urlAppend = "?page[offset]=";
			if (request.contains("?")) {
				urlAppend = "&page[offset]=";
			}
			firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request)).data;
			list = firstFifty;
			counter = 50;
			while (firstFifty.size() == 50 && counter < 2000) {
				firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request + urlAppend + counter)).data;
				list.addAll(firstFifty);
				counter = counter + 50;

			}
		}
		return list;
	}

	public String httpGetFromEvint(String urlString) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
		}
	}




}

