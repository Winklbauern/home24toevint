package tcon.employee.groovy

import tcon.util.Message

public class EmployeeToEvint {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public EmployeeToEvint(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		this.processData(message);
	}


	Message processData(Message message) {

		//TRACER:
		def tracer = true;
		def sTracer = "";

		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);

		//catch them all
		try {

			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();
			//sfEmployees = jsonSlurper.parseText(body).queryCompoundEmployeeResponse.CompoundEmployee;

			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			sfEmployees = jsonSlurper.parseText(message.getBody("AllSfEmployees"));
			//**CLOUD SETTINGS*******************************************************************





			def evintFifty = jsonSlurper.parseText(this.httpGet("https://evintsnsrv01.gbg-ag.com/api/pdemo/public/employees", messageLog)).data;
			def evintEmployees = evintFifty;

			//Evint API only return 50 Entry
			def counter = 50;
			while (evintFifty.size() == 50) {
				evintFifty = jsonSlurper.parseText(this.httpGet("https://evintsnsrv01.gbg-ag.com/api/pdemo/public/employees?page[offset]=" + counter, messageLog)).data;
				evintEmployees.addAll(evintFifty);
				counter = counter + 50;
			}
			//if(tracer){
			//messageLog.addAttachmentAsString("AllSFEmployees"+sfEmployees.size(), JsonOutput.toJson(sfEmployees), "text/plain");
			//messageLog.addAttachmentAsString("AllEvintEmployees"+evintEmployees.size(), JsonOutput.toJson(evintEmployees), "text/plain");};
			def start = new Date();
			//for (i = 100; i < 300; i++) {

			for (def i = 0; i < sfEmployees.size(); i++) {

				def sfEmployee = sfEmployees.get(i).person; //2016
				def personalNumber = sfEmployee.person_id_external;
				def evintEmployee = null;
				try {
					def personalInformation = this.getCorrectObject(sfEmployee.personal_information);
					def employmentInformation = this.getCorrectObject(sfEmployee.employment_information);
					def nationalIdCard = this.getCorrectObject(sfEmployee.national_id_card);
					def job_information = this.getCorrectObject(employmentInformation.job_information);


					//do only for this person
					//if(!sfEmployee.person_id_external.equals("5041")){continue;}


					if(tracer){sTracer = sTracer + "\nPersonID:"+sfEmployee.person_id_external};

					//do not for
					//if (sfEmployee.person_id_external.equals("2641") || sfEmployee.person_id_external.equals("3470") || sfEmployee.person_id_external.equals("3528") || sfEmployee.person_id_external.equals("3537")) {
					//  continue;
					//}


					//def startDate = new Date().parse("yyyy-MM-dd", personalInformation.start_date);
					//def endDate = new Date().parse("yyyy-MM-dd", personalInformation.end_date);




					def lastModified = this.getHighestDate([sfEmployee, personalInformation, employmentInformation, nationalIdCard, job_information]);
					def evintEmployeeAttributes = null;

					for (def j = 0; j < evintEmployees.size(); j++) {
						evintEmployeeAttributes = evintEmployees.get(j).attributes;
						if (personalNumber.equals(evintEmployeeAttributes.personnelNumber)) {
							evintEmployee = evintEmployees.get(j);
							break;
						}
						evintEmployeeAttributes = null;
					}

					// Look for corresponding evint employee
					def updateMap = [: ];
					def updateUrl = "";

					if (evintEmployeeAttributes != null) {
						if(tracer){sTracer = sTracer + "\tPATCH"};

						//Ausgabe von beiden
						//messageLog.addAttachmentAsString("sfEmployee", JsonOutput.toJson(sfEmployee), "text/plain");
						//messageLog.addAttachmentAsString("EvintEmployee", JsonOutput.toJson(evintEmployee), "text/plain");
						if (evintEmployeeAttributes.modifiedOn == null || lastModified >= new Date().parse("yyyy-MM-dd HH:mm:ss", evintEmployeeAttributes.modifiedOn)) {
							updateUrl = evintEmployee.links.self;

							if (evintEmployeeAttributes.dateOfBirth == null || !sfEmployee.date_of_birth.equals(evintEmployeeAttributes.dateOfBirth)) {
								updateMap.put("dateOfBirth", sfEmployee.date_of_birth);
							}

							if (!personalInformation.first_name.toLowerCase().equals(evintEmployeeAttributes.firstName.toLowerCase())) {
								updateMap.put("firstName", personalInformation.first_name);
							}

							if (!personalInformation.last_name.toLowerCase().equals(evintEmployeeAttributes.lastName.toLowerCase())) {
								updateMap.put("lastName", personalInformation.last_name);
							}


							if (nationalIdCard && nationalIdCard.essn && (!nationalIdCard.essn.equals(evintEmployeeAttributes.socialSecurityNumber))) {
								updateMap.put("socialSecurityNumber", nationalIdCard.essn);
							}

							if ((personalInformation.gender.equals("M") && !evintEmployeeAttributes.salutation.equals("1")) || (personalInformation.gender.equals("F") && !evintEmployeeAttributes.salutation.equals("2"))) {
								def gender = personalInformation.gender.equals("M") ? 1 : 2;
								updateMap.put("salutation", gender);
							}
							def dataMap = [: ];
							def attributeMap = [: ];
							attributeMap.put("id", evintEmployee.id);
							attributeMap.put("type", "employees");
							attributeMap.put("attributes", updateMap);
							dataMap.put("data", attributeMap);
							if(tracer){sTracer = sTracer + "\tpatchMap:"+JsonOutput.toJson(dataMap)};
							this.httpPatch(updateUrl, JsonOutput.toJson(dataMap), messageLog, "PATCH");
						}else{
							if(tracer){sTracer = sTracer + "\t BEREITS geupdatet, SF modifiedOn:"+lastModified + "\tEvint modifiedOn:"+evintEmployeeAttributes.modifiedOn};
						}
					}
					else {
						if(tracer){sTracer = sTracer + "\tPOST"};

						updateUrl = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/employees";
						def entryDate = employmentInformation.start_date;
						def location = "";
						def dataMap = [: ];
						def attributeMap = [: ];
						location = job_information.location;

						def relationMap = [: ],
						subMap = [: ],
						dataSubMap = [: ];
						dataSubMap.put("type", "subsidiaries");
						if (!this.getSubId(location)) {
							if(tracer){sTracer = sTracer + "\t Location gibt es nicht in Evint:"+location};
							continue;
						}
						dataSubMap.put("id", this.getSubId(location));
						subMap.put("data", dataSubMap);
						relationMap.put("subsidiaries", subMap);
						attributeMap.put("relationships", relationMap);

						def gender = personalInformation.gender.equals("M") ? 1 : 2

						updateMap.put("firstName", personalInformation.first_name);
						updateMap.put("lastName", personalInformation.last_name);
						updateMap.put("dateOfBirth", sfEmployee.date_of_birth);
						updateMap.put("personnelNumber", personalNumber);
						updateMap.put("entryDate", entryDate);
						//updateMap.put("separationDate", entryDate);
						if (nationalIdCard && nationalIdCard.essn) {
							updateMap.put("socialSecurityNumber", nationalIdCard.essn);
						}

						updateMap.put("salutation", gender);
						//updateMap.put("active", true);
						//updateMap.put("deactivatedOn", null);
						attributeMap.put("type", "employees");
						attributeMap.put("attributes", updateMap);

						dataMap.put("data", attributeMap);
						if(tracer){sTracer = sTracer + "\tpostMap:"+JsonOutput.toJson(dataMap)};
						this.httpPatch(updateUrl, JsonOutput.toJson(dataMap), messageLog, "POST");

					}
				} catch(Exception ex) {
					def exceptionOutput = "Exceptionmessage:\n" + ex.toString() + "\n\nStackTrace:";
					def aStack = ex.getStackTrace();
					for(def j = 0;j<aStack.size();j++) {
						def oStack = aStack[j];
						if(oStack.toString().contains("tcon")) {
							exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
						}
					}
					if (sfEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nSFEmployee:\n" + JsonOutput.toJson(sfEmployee);
					}
					if (evintEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nEvintEmployee:\n" + JsonOutput.toJson(evintEmployee);
					}
					if(tracer){sTracer = sTracer + "\nERROR für "+personalNumber+" :"+exceptionOutput};
				}


			}
		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}
		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};


		return message;
	}

	public String getSubId(String subSf) {
		if (subSf.equals("HS")) {
			return "f65bf9b1-d8df-4908-88ec-44db1b4d9392";
		} else if (subSf.equals("LF")) {
			return "6460fc9e-d353-4c93-8cb5-4d88725d14f2";
		} else if (subSf.equals("WR")) {
			return "54aeabc2-efb1-4342-82dd-419601c80c8e";
		} else {
			return null;
		}
	}

	public String httpGet(String urlString, messageLog) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
		}


	}

	public String httpPatch(String urlString, String json, messageLog, String type) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		if (type.equals("PATCH")) {
			urlConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
		}
		urlConnection.setRequestMethod("POST");
		urlConnection.setDoOutput(true);
		urlConnection.getOutputStream().write(json.getBytes("UTF-8"));
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\n" + type + " " + urlString + "\n" + json);
		}
	}

	public Date getHighestDate(arrayObject){

		def lastModified = null;
		for (def i = 0; i < arrayObject.size(); i++) {
			if(arrayObject[i] == null || arrayObject[i].last_modified_on == null){
				continue;
			}
			def entryLastModified = new Date().parse("yyyy-MM-dd HH:mm:ss", arrayObject[i].last_modified_on.substring(0,19).replace("T"," "));
			if(lastModified == null || lastModified.getTime() < entryLastModified.getTime()){
				lastModified = entryLastModified;
			}
		}
		return lastModified;
	}


	public Object getCorrectObject(Object object) {
		if (object instanceof java.util.ArrayList) {
			for (def i = 0; i < object.size(); i++) {
				def oEntity = object[i];
				def start = new Date().parse("yyyy-MM-dd", oEntity.start_date);
				def end = null;
				//if enddate not filled --> 9999-12-12
				if (oEntity.end_date) {
					end = new Date().parse("yyyy-MM-dd", oEntity.end_date);
				} else {
					end = new Date().parse("yyyy-MM-dd", "9999-12-31");
				}
				def now = new Date();
				def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());
				if (start.getTime() <= dayNow.getTime() && end.getTime() >= dayNow.getTime()) {
					return oEntity;
				}
			}
		} else {
			return object;
		}
	}
}

