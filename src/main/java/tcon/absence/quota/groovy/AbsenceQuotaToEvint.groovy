package tcon.absence.quota.groovy

import tcon.util.Message

public class AbsenceQuotaToEvint {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public AbsenceQuotaToEvint(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		this.processData(message);
	}


	Message processData(Message message) {

		//Url for Evint-API
		def url = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/";

		//***********************************************************************************
		// TRACER Declaration
		//***********************************************************************************
		def tracer = true;
		def sTracer = "";
		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);

		//catch them all
		try {


			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;
			def evintEmployees = null;
			def absenceQuotaSf = null;
			def absenceQuotaEvint = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();
			//sfEmployees = jsonSlurper.parseText(body).queryCompoundEmployeeResponse.CompoundEmployee;


			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			sfEmployees = jsonSlurper.parseText(message.getBody("AllSfEmployees"));
			//		evintEmployees = jsonSlurper.parseText(message.getBody("absence\\quota\\files\\","AllEvintEmployees"));
			//		absenceQuotaSf = jsonSlurper.parseText(message.getBody("absence\\quota\\files\\","AllSfAbsenceQuota"));
			//		absenceQuotaEvint = jsonSlurper.parseText(message.getBody("absence\\quota\\files\\","AllEvintAbsenceQuota"));
			//**CLOUD SETTINGS*******************************************************************


			evintEmployees = this.getList(url + "employees", jsonSlurper);
			absenceQuotaSf = this.getList("https://api12preview.sapsf.eu/odata/v2/TimeAccount?%24expand=timeAccountDetails",jsonSlurper);
			absenceQuotaEvint = this.getList(url + "holidayAccountEntitlements?include=employees", jsonSlurper);

			//		messageLog.addAttachmentAsString("AllSfEmployees",JsonOutput.toJson(sfEmployees),"text/plain");
			//		messageLog.addAttachmentAsString("AllEvintEmployees", JsonOutput.toJson(evintEmployees), "text/plain");
			//		messageLog.addAttachmentAsString("AllSfAbsenceQuota", JsonOutput.toJson(absenceQuotaSf), "text/plain");
			//		messageLog.addAttachmentAsString("AllEvintAbsenceQuota", JsonOutput.toJson(absenceQuotaEvint), "text/plain");


			def aSfAbscenceQuotas = [:];
			for(def i = 0; i<absenceQuotaSf.size();i++) {

				def oAbsence = absenceQuotaSf[i];
				def sId = oAbsence["userId"];
				def aAbsenceFromMap = aSfAbscenceQuotas.get(sId);

				if(aAbsenceFromMap == null){
					aAbsenceFromMap = [];
					aAbsenceFromMap.push(oAbsence);
					aSfAbscenceQuotas.put(sId,aAbsenceFromMap);
				}else{
					aAbsenceFromMap.push(oAbsence);
					aSfAbscenceQuotas.put(sId,aAbsenceFromMap);
				}
			}
			def aEvintAbscenceQuotas = [:];
			for(def i = 0; i<absenceQuotaEvint.size();i++) {
				def oAbsence = absenceQuotaEvint[i];
				def sId = oAbsence["relationships"]["employees"]["data"]["id"];
				def aAbsenceFromMap = aEvintAbscenceQuotas.get(sId);
				if(aAbsenceFromMap == null){
					aAbsenceFromMap = [];
					aAbsenceFromMap.push(oAbsence);
					aEvintAbscenceQuotas.put(sId,aAbsenceFromMap);
				}else{
					aAbsenceFromMap.push(oAbsence);
					aEvintAbscenceQuotas.put(sId,aAbsenceFromMap);
				}
			}


			for(def i = 0; i<sfEmployees.size(); i++){
				def sfEmployee = sfEmployees.get(i).person;
				def employmentInformation = this.getCorrectObject(sfEmployee.employment_information);
				def jobInfo = this.getCorrectObject(employmentInformation.job_information);
				//is User Active?
				if (! (sfEmployee.logon_user_is_active.toBoolean())) {
					continue;
				}

				//do only for this person
				//			if (! (sfEmployee.person_id_external.equals("3921") )) {
				//				continue;
				//			}
				def personalNumber = null;
				def evintEmpId = null;
				try {
					personalNumber = sfEmployee.person_id_external;
					if (tracer) {
						sTracer = sTracer + "\nPersonIDSf:" + personalNumber;
					};

					//***********************************************************************************
					//exist Candidate in SF?
					//***********************************************************************************

					for (def j = 0; j < evintEmployees.size(); j++) {
						if (evintEmployees[j]["attributes"]["personnelNumber"].equals(personalNumber)) {
							evintEmpId = evintEmployees[j]["id"];
						}
					}
					if (!evintEmpId) {
						//if location = GWS --> candidate actually not inserted in Evint
						if (! (jobInfo && jobInfo.location && jobInfo.location.equals("GWS"))) {
							if (tracer) {
								sTracer = sTracer + "\tEmployee not Exist in Evint";
							}
							continue;
						} else if(jobInfo.location.equals("GWS") ){
							if (tracer) {
								sTracer = sTracer + "\tCandidate has Location GWS";
							}
							continue;
						}
					}
					if (tracer) {
						sTracer = sTracer + "\tPersonIDEvint:" + evintEmpId;
					};
					def aQuotaSf = [], personId = sfEmployee.person_id,absence = null, aQuotaInSum = [:];

					def aQuotaEvint = [];
					if (tracer) {
						sTracer = sTracer + "\n\tEvintquota:";
					};
					def aAbsenceQuotaEvint = aEvintAbscenceQuotas.get(evintEmpId);
					if(aAbsenceQuotaEvint != null) {
						for(def j = 0; j<aAbsenceQuotaEvint.size(); j++){
							absence = aAbsenceQuotaEvint[j];
							aQuotaEvint.add(absence);
							if (tracer) {
								sTracer = sTracer + "\n\t\tYear:"+absence["attributes"]["validFrom"]+"\tAmount:"+absence["attributes"]["annualLeaveEntitlement"];
							};

						}
					}

					if (tracer) {
						sTracer = sTracer + "\n\n\tSFquota:";
					};
					def aAbsenceQuotaSf = aSfAbscenceQuotas.get(personalNumber);

					if(aAbsenceQuotaSf != null) {
						for(def j = 0; j<aAbsenceQuotaSf.size(); j++){
							absence = aAbsenceQuotaSf[j];
							if(absence["accountType"].equals("Urlaub")){
								def aDetails = absence["timeAccountDetails"]["results"];
								def sYear = this.getYear(absence["bookingStartDate"]);
								for(def k = 0; k<aDetails.size();k++){

									def oDetail = aDetails[k];

									if(!(oDetail["bookingType"].equals("RECALCULATION") ||oDetail["bookingType"].equals("ACCRUAL"))) {
										continue;
									}
									def sAmount = oDetail["bookingAmount"];
									def oYearEntry = aQuotaInSum.get(sYear);
									if(oYearEntry == null){
										aQuotaInSum.put(sYear,sAmount);
									}else{
										def dSum = Double.parseDouble(oYearEntry) + Double.parseDouble(sAmount);
										aQuotaInSum.put(sYear,dSum+"");
									}
								}
								aQuotaSf.add(absence);
								if (tracer) {
									sTracer = sTracer + "\n\t\tYear:"+sYear+"\tAmount:"+aQuotaInSum.get(sYear);
								};
							}
						}
					}


					for(def j=0; j<aQuotaSf.size();j++){
						def oQuota = aQuotaSf[j];
						def sYear = this.getYear(oQuota["bookingStartDate"]);
						def sSum = aQuotaInSum.get(sYear);
						def bIsExisting = false;
						for(def k=0; k<aQuotaEvint.size();k++){
							def oAbsenceEvint = aQuotaEvint[k];
							if(oAbsenceEvint["attributes"]["validFrom"].equals(sYear)){
								bIsExisting = true;
								break;
							}
						}
						if(!bIsExisting && sSum != null){
							def requestMap = [: ],
							dataMap = [: ],
							attributesMap = [: ],
							relationshipsMap = [: ],
							employeesMap = [: ],
							employeesDataMap = [: ];

							//employees
							employeesDataMap.put("type", "employees");
							employeesDataMap.put("id", evintEmpId);
							employeesMap.put("data", employeesDataMap);

							relationshipsMap.put("employees", employeesMap);

							attributesMap.put("validFrom", sYear);
							attributesMap.put("validTo", sYear);
							//Evint dont accept no Doubles with .00 or .0, only if Digits
							//are filled > 0
							//10.0 --> 10
							if(this.isDigitZero(Double.parseDouble(sSum))) {
								int iSum = Double.parseDouble(sSum);
								sSum = iSum + "";
							}
							if(Double.parseDouble(sSum) == 0.0) {
								continue;
							}
							attributesMap.put("annualLeaveEntitlement", sSum);


							dataMap.put("type", "holidayAccountEntitlements");
							dataMap.put("attributes", attributesMap);
							dataMap.put("relationships", relationshipsMap);

							requestMap.put("data", dataMap);
							if (tracer) {
								sTracer = sTracer + "\n\n\tUPDATE:\tYear:"+sYear+"\tAmount:"+sSum;
							};
							this.httpPatch(url + "holidayAccountEntitlements", JsonOutput.toJson(requestMap), messageLog, "POST");
						}
						//sTracer = sTracer + "\nsYear:\t"+sYear+"\tAmount"+sSum;
					}


				} catch(Exception ex) {
					def exceptionOutput = "Exceptionmessage:\n" + ex.toString() + "\n\nStackTrace:";
					def aStack = ex.getStackTrace();
					for(def j = 0;j<aStack.size();j++) {
						def oStack = aStack[j];
						if(oStack.toString().contains("tcon")) {
							exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
						}
					}
					if (sfEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nSFEmployee:\n" + JsonOutput.toJson(sfEmployee);
					}
					if (evintEmpId != null) {
						exceptionOutput = exceptionOutput + "\n\nEvintEmployee:\n" + JsonOutput.toJson(evintEmpId);
					}
					if(tracer){sTracer = sTracer + "\nERROR für "+personalNumber+" :"+exceptionOutput};
				}

			}
		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}
		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};

		return message;

	}





	public boolean isDigitZero(Double dDouble) {

		int iDouble = dDouble;
		dDouble = dDouble - iDouble;
		if(dDouble == 0.0){
			return true;
		} else {
			return false
		}
	}

	public String getYear(String date){
		return (new Date(Long.valueOf(date.replace("/Date(","").replace(")/",""))).getYear()+1900)+"";
	}


	public Object getList(String request, jsonSlurper) {
		def firstFifty = null,
		urlAppend = null,
		counter = null,
		list = null;
		if(request.contains("evint")){
			urlAppend = "?page[offset]=";
			if (request.contains("?")) {
				urlAppend = "&page[offset]=";
			}
			firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request)).data;
			list = firstFifty;
			counter = 50;
			while (firstFifty.size() == 50) {
				firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request + urlAppend + counter)).data;
				list.addAll(firstFifty);
				counter = counter + 50;
			}
		} else{

			if(request.contains("?")){
				urlAppend = "&%24skip=";
			} else{
				urlAppend = "?%24skip=";
			}

			firstFifty = jsonSlurper.parseText(this.httpGetFromSf(request)).d.results;
			list = firstFifty;
			counter = 1000;

			while (firstFifty.size() == 1000) {
				firstFifty = jsonSlurper.parseText(this.httpGetFromSf(request + urlAppend + counter)).d.results;
				list.addAll(firstFifty);
				counter = counter + 1000;
			}
		}
		//Evint API only return 50 Entry and SF only 1000


		return list;
	}

	public String httpGetFromSf(String urlString) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic VENPTkFETUlOXzAwMUBob21lc2VUMTpUQ09OQ0hkZTIwMjEh");
		urlConnection.setRequestProperty("Accept", "application/json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
		}
	}



	public String httpGetFromEvint(String urlString) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpGetFromEvint(urlString);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\nGET " + urlString);
			}
			
		}
	}


	public String httpPatch(String urlString, String json, messageLog, String type) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		if (type.equals("PATCH")) {
			urlConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
		}
		urlConnection.setRequestMethod("POST");
		urlConnection.setDoOutput(true);
		urlConnection.getOutputStream().write(json.getBytes("UTF-8"));
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpPatch(urlString, json, messageLog, type);
			} else{
				throw new Exception("Message from Request:\n" + urlConnection.getErrorStream().getText() + "\nRequest:\n" + type + " " + urlString + "\n" + json);
			}
			
		}
	}
	public Object getCorrectObject(Object object) {
		if (object instanceof java.util.ArrayList) {
			for (def i = 0; i < object.size(); i++) {
				def oEntity = object[i];
				def start = new Date().parse("yyyy-MM-dd", oEntity.start_date);
				def end = null;
				//if enddate not filled --> 9999-12-12
				if (oEntity.end_date) {
					end = new Date().parse("yyyy-MM-dd", oEntity.end_date);
				} else {
					end = new Date().parse("yyyy-MM-dd", "9999-12-31");
				}
				def now = new Date();
				def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());
				if (start.getTime() <= dayNow.getTime() && end.getTime() >= dayNow.getTime()) {
					return oEntity;
				}
			}
		} else {
			return object;
		}

	}



}
