package tcon.absence.absence.groovy

import tcon.util.Message


public class AbsenceToEvint {

	def jsonSlurper = null;

	def JsonOutput = null;

	def message = null;

	def messageLogFactory = null;

	public AbsenceToEvint(jsonSlurper, JsonOutput, message) {
		this.jsonSlurper = jsonSlurper;
		this.JsonOutput = JsonOutput;
		this.message = message;
		this.messageLogFactory = message;
	}

	public Message doAll() {
		this.processData(message);
	}

	Message processData(Message message) {

		//Url for Evint-API
		def sEvintUrl = "https://evintsnsrv01.gbg-ag.com/api/pdemo/public/",
		sSfUrl = "https://api12preview.sapsf.eu/odata/v2/";

		//***********************************************************************************
		// TRACER Declaration
		//***********************************************************************************
		def tracer = true;
		def sTracer = "";
		def body = message.getBody(java.lang.String.class);
		def messageLog = messageLogFactory.getMessageLog(message);
		//catch them all
		try {


			//**CLOUD SETTINGS*******************************************************************
			def jsonSlurper = null;
			def sfEmployees = null;
			def absenceSf = null;
			def absenceTypesSf = null;
			def evintEmployees = null;
			def absenceEvint = null
			def absenceTypesEvint = null;

			//for CLOUD:
			//jsonSlurper = new JsonSlurper();
			//sfEmployees = jsonSlurper.parseText(body).queryCompoundEmployeeResponse.CompoundEmployee;

			//for LOCAL Eclipse
			jsonSlurper = this.jsonSlurper;
			sfEmployees = jsonSlurper.parseText(message.getBody("AllSfEmployees"));


			//for LOCAL Performance
			//absenceEvint= jsonSlurper.parseText(message.getBody("AllEvintAbsences"));
			//absenceSf = jsonSlurper.parseText(message.getBody("AllSfAbsences"));
			//evintEmployees = jsonSlurper.parseText(message.getBody("AllEvintEmployees"));
			//**CLOUD SETTINGS*******************************************************************


			//***********************************************************************************
			//get Stuff from APIS (SFEmps, TimeAccounts/AbsenceQuota ;-), Evint Quota)
			//***********************************************************************************
			//messageLog.addAttachmentAsString("AllSfEmployees",JsonOutput.toJson(sfEmployees),"text/plain");

			evintEmployees = this.getList(sEvintUrl + "employees", jsonSlurper);
			//messageLog.addAttachmentAsString("AllEvintEmployees", JsonOutput.toJson(evintEmployees), "text/plain");



			def oNowMinusThreeMonths = new Date();
			//add 1 day for next call
			Calendar cal = Calendar.getInstance();
			cal.setTime(oNowMinusThreeMonths);
			cal.add(Calendar.MONTH, -3);


			def requestMap = [: ],
			expressionMap = [: ];
			expressionMap.put("attribute", "date");
			expressionMap.put("comparison", "gte");
			expressionMap.put("value", cal.getTime().format("yyyy-MM-dd"));
			requestMap.put("operation", "and");
			requestMap.put("expressions", [expressionMap]);
			def sFilter = JsonOutput.toJson(requestMap).bytes.encodeBase64().toString();
			absenceEvint = this.getList(sEvintUrl + "absences?include=absenceTypes,employees&filter="+sFilter, jsonSlurper);
			messageLog.addAttachmentAsString("AllEvintAbsences", JsonOutput.toJson(absenceEvint), "text/plain");


			sFilter = "?%24filter=startDate gt '"+cal.getTime().format("yyyy-MM-dd") +"'";

			absenceSf = this.getList(sSfUrl+"EmployeeTime"+sFilter,jsonSlurper);
			messageLog.addAttachmentAsString("AllSfAbsences", JsonOutput.toJson(absenceSf), "text/plain");





			//***********************************************************************************
			//MAP EmplId to Absences for Performance!!!11!
			//***********************************************************************************

			def aSfAbscences = [:];
			for(def i = 0; i<absenceSf.size();i++) {
				def oAbsence = absenceSf[i];
				def sId = oAbsence["userId"];
				def aAbsenceFromMap = aSfAbscences.get(sId);
				if(aAbsenceFromMap == null){
					aAbsenceFromMap = [];
					aAbsenceFromMap.push(oAbsence);
					aSfAbscences.put(sId,aAbsenceFromMap);
				}else{
					aAbsenceFromMap.push(oAbsence);
					aSfAbscences.put(sId,aAbsenceFromMap);
				}
			}
			def aEvintAbscences = [:];
			//this.deleteDupplicates(absenceEvint);
			for(def i = 0; i<absenceEvint.size();i++) {
				def oAbsence = absenceEvint[i];
				def sId = oAbsence["relationships"]["employees"]["data"]["id"];
				def aAbsenceFromMap = aEvintAbscences.get(sId);
				if(aAbsenceFromMap == null){
					aAbsenceFromMap = [];
					aAbsenceFromMap.push(oAbsence);
					aEvintAbscences.put(sId,aAbsenceFromMap);
				}else{
					//getMap deletes Dupplicates --> fck Evint
					aAbsenceFromMap = this.getMap(aAbsenceFromMap, oAbsence);
					aEvintAbscences.put(sId,aAbsenceFromMap);
				}
			}


			for(def i = 0; i<sfEmployees.size(); i++){

				def sfEmployee = sfEmployees.get(i).person;
				def employmentInformation = this.getCorrectObject(sfEmployee.employment_information);
				def jobInfo = this.getCorrectObject(employmentInformation.job_information);

				//is User Active?
				if (! (sfEmployee.logon_user_is_active.toBoolean())) {
					continue;
				}

				//do only for this person
				//				if (! (sfEmployee.person_id_external.equals("4300") )) {
				//					continue;
				//				}
				//							if (! (sfEmployee.person_id_external.equals("3330") )) {
				//								continue;
				//							}

				def personalNumber = null;
				def evintEmpId = null;
				try {
					personalNumber = sfEmployee.person_id_external;
					if (tracer) {
						sTracer = sTracer + "\n\n\n\nPersonIDSf:" + personalNumber;
					};
					//***********************************************************************************
					//exist Candidate in SF?
					//***********************************************************************************
					for (def j = 0; j < evintEmployees.size(); j++) {

						if (evintEmployees[j]["attributes"]["personnelNumber"].equals(personalNumber)) {
							evintEmpId = evintEmployees[j]["id"];
						}
					}
					if (!evintEmpId) {
						//if location = GWS --> candidate actually not inserted in Evint
						if (! (jobInfo && jobInfo.location && jobInfo.location.equals("GWS"))) {
							if (tracer) {
								sTracer = sTracer + "\tEmployee not Exist in Evint";
							}
							continue;
						} else if(jobInfo.location.equals("GWS") ){
							if (tracer) {
								sTracer = sTracer + "\tCandidate has Location GWS";
							}
							continue;
						}
					}
					if (tracer) {
						sTracer = sTracer + "\tPersonIDEvint:" + evintEmpId;
					};


					//***********************************************************************************
					//Logic for Creating
					//***********************************************************************************
					
					def aAbsenceFromSf = aSfAbscences.get(personalNumber);
					def aAbsenceFromEvint = aEvintAbscences.get(evintEmpId);
					println JsonOutput.toJson(aAbsenceFromSf);
					if(aAbsenceFromSf != null) {
						for(def j = 0; j<aAbsenceFromSf.size();j++) {
							def oAbsence = aAbsenceFromSf[j];
							//Absence not approved --> accepted
							if(!oAbsence["approvalStatus"].equals("APPROVED")) {
								continue;
							}
							Date oFirstDate = this.getDate(oAbsence["startDate"]),
							oEndDate = this.getDate(oAbsence["endDate"]);
							//in Evint, for whatever reason,
							//there is only a day vacation, so no time period ^^
							//if --> more days
							//else --> one or less
							Double dHours = null;
							String sTimeType = oAbsence["timeType"];
							if(Double.parseDouble(oAbsence["quantityInDays"]) > 1.0) {
								dHours = Double.parseDouble(oAbsence["quantityInHours"]) / Double.parseDouble(oAbsence["quantityInDays"]);
								while(oFirstDate.getTime()<=oEndDate.getTime()) {
									//CHECK IF DAY EXIST IN EVINT
									if(aAbsenceFromEvint != null && this.isDayExistingInEvint(oFirstDate, aAbsenceFromEvint)) {
										oFirstDate = this.addDayToDate(oFirstDate);
									}else {
										sTracer = sTracer + this.doCreateAbsence(oFirstDate,dHours,evintEmpId,this.getAbsTypeEvintId(sTimeType),sEvintUrl,messageLog,sTracer );
										oFirstDate = this.addDayToDate(oFirstDate);
									}
								}
							} else {
								if(aAbsenceFromEvint != null && this.isDayExistingInEvint(oFirstDate, aAbsenceFromEvint)) {
									continue;
								}
								dHours = Double.parseDouble(oAbsence["quantityInHours"]);
								sTracer = sTracer + this.doCreateAbsence(oFirstDate,dHours,evintEmpId,this.getAbsTypeEvintId(sTimeType),sEvintUrl,messageLog,sTracer );
							}
						}
					}
					//***********************************************************************************
					//Logic for Deleting in Evint if in SuccesFactors dont Exist
					//***********************************************************************************
					// 3 Steps to check:
					// 1. Is Date in SF Existing?
					// 2. Same Hours?
					// 3. Same Type?
					// 4. Is Approved in SF?
					if(aAbsenceFromEvint != null) {
						for(def j = 0; j<aAbsenceFromEvint.size();j++) {
							def bIsAbsenceToDelete = false;

							def oAbsence = aAbsenceFromEvint[j];
							Date oDate = Date.parse("yyyy-MM-dd",oAbsence["attributes"]["date"]);
							Double dHours = Double.parseDouble(oAbsence["attributes"]["hours"]);
							String sTimeType = this.getAbsTypeSfId(oAbsence["relationships"]["absenceTypes"]["data"]["id"]);
							// 1. Step Is Date in SF Existing
							def oSfAbsence = this.isDayExistingInSf(oDate, aAbsenceFromSf);
							if(oSfAbsence == null) {
								sTracer = sTracer + "\nDELETE:1.";
								bIsAbsenceToDelete = true;
							} else {
								// 2. Step Same Hours
								if(!((Double.parseDouble(oSfAbsence["quantityInHours"]) / Double.parseDouble(oSfAbsence["quantityInDays"])) == dHours)) {
									sTracer = sTracer + "\nDELETE:2.";
									bIsAbsenceToDelete = true;
								} 
								// 3. Step Same TimeType
								else if(!(oSfAbsence["timeType"].equals(sTimeType))) {
									sTracer = sTracer + "\nDELETE:3.";
									bIsAbsenceToDelete = true;
								}
								// 4. Step Is Approved In SF
								else if(!oSfAbsence["approvalStatus"].equals("APPROVED")){
									sTracer = sTracer + "\nDELETE:4.";
									bIsAbsenceToDelete = true;
								}
								else {
									sTracer = sTracer + "\nExist!!!!"+oAbsence.toString();
								}
							}
							if(bIsAbsenceToDelete) {
								//delete
								this.httpDelete(oAbsence["links"]["self"], messageLog);
								sTracer = sTracer + "\nDELETE:\n"+JsonOutput.toJson(oAbsence);
							}
						}
					}

				} catch(Exception ex) {
					def exceptionOutput = "Exceptionmessage:\n" + ex.toString() + "\n\nStackTrace:";
					def aStack = ex.getStackTrace();
					for(def j = 0;j<aStack.size();j++) {
						def oStack = aStack[j];
						if(oStack.toString().contains("tcon")) {
							exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
						}
					}
					if (sfEmployee != null) {
						exceptionOutput = exceptionOutput + "\n\nSFEmployee:\n" + JsonOutput.toJson(sfEmployee);
					}
					if (evintEmpId != null) {
						exceptionOutput = exceptionOutput + "\n\nEvintEmployee:\n" + JsonOutput.toJson(evintEmpId);
					}
					if(tracer){sTracer = sTracer + "\nERROR für "+personalNumber+" :"+exceptionOutput};
				}

			}
			if(tracer){sTracer = "ERFOLGREICH durchgelaufen" +sTracer};
		} catch(Exception exception) {
			def exceptionOutput = "Exceptionmessage:\n" + exception.toString() + "\n\nStackTrace:";
			def aStack = exception.getStackTrace();
			for(def j = 0;j<aStack.size();j++) {
				def oStack = aStack[j];
				if(oStack.toString().contains("tcon")) {
					exceptionOutput = exceptionOutput + "\n\t" +oStack.toString();
				}
			}
			if(tracer){sTracer = sTracer + "\nERROR:"+exceptionOutput};
		}

		if(tracer){messageLog.addAttachmentAsString("TRACELOG", sTracer, "text/plain");};
		return message;
	}

	public String doCreateAbsence(oFirstDate,dHours,evintEmpId, sTimeType,sUrl,messageLog,sTracer ) {
		def requestMap = [: ],
		dataMap = [: ],
		attributesMap = [: ],
		relationshipsMap = [: ],
		employeesMap = [: ],
		employeesDataMap = [: ],
		absenceTypeMap = [:],
		absenceTypeDataMap=[:];

		//employees
		employeesDataMap.put("type", "employees");
		employeesDataMap.put("id", evintEmpId);
		employeesMap.put("data", employeesDataMap);

		absenceTypeDataMap.put("type", "absenceTypes");
		absenceTypeDataMap.put("id", sTimeType);
		absenceTypeMap.put("data", absenceTypeDataMap);

		relationshipsMap.put("absenceTypes", absenceTypeMap);
		relationshipsMap.put("employees", employeesMap);

		attributesMap.put("date", oFirstDate.format("yyyy-MM-dd"));
		attributesMap.put("hours", dHours);


		dataMap.put("type", "absences");
		dataMap.put("attributes", attributesMap);
		dataMap.put("relationships", relationshipsMap);

		requestMap.put("data", dataMap);
		try {
			this.httpPatch(sUrl + "absences", JsonOutput.toJson(requestMap), messageLog, "POST");
			return  "\n\t\tSuccess POST:"+JsonOutput.toJson(requestMap);
		} catch(Exception e) {
			//nothing, bis schepert und kracht;
			return "\n\t\tERROR:"+e.getMessage();
			//throw e;
		}

	}

	public Date addDayToDate(Date oDate) {
		//add 1 day for next call
		Calendar cal = Calendar.getInstance();
		cal.setTime(oDate);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public Object isDayExistingInSf(oDate, aAbsences) {
		def sFormat = "yyyy-MM-dd";
		for(def i = 0; i<aAbsences.size();i++) {
			def oAbsence = aAbsences[i];
			Date oFirstDate = this.getDate(oAbsence["startDate"]),
			oEndDate = this.getDate(oAbsence["endDate"]);
			if(Date.parse(sFormat,oDate.format("yyyy-MM-dd")).getTime() >= Date.parse(sFormat,oFirstDate.format("yyyy-MM-dd")).getTime() && Date.parse(sFormat,oDate.format("yyyy-MM-dd")).getTime()<=Date.parse(sFormat,oEndDate.format("yyyy-MM-dd")).getTime()) {
				return oAbsence;
			}
		}
		return null;
	}

	public boolean isDayExistingInEvint(oDate, aAbsences) {
		for(def i = 0; i<aAbsences.size();i++) {
			def oAbsence = aAbsences[i];
			if(oAbsence["attributes"]["date"].equals(oDate.format("yyyy-MM-dd"))) {
				return true;
			}
		}
		return false;
	}

	public Object getMap(aArray, oObject) {
		def bIsExisting = false;
		for(def j = 0;j<aArray.size();j++) {
			if (aArray[j]["id"].equals(oObject["id"])) {
				bIsExisting = true;
			}
		}
		if(!bIsExisting) {
			aArray.push(oObject);
		}
		return aArray;
	}

	public String getAbsTypeSfId(String sEvintId) {
		if(sEvintId.equals("7747b6ed-5991-4756-9d74-a0154431a4bf")){
			return "Ausgleichstag";
		}
		if(sEvintId.equals("1a9d711b-0807-44f6-b307-74df19e6ef11")){
			return "Beschäftigungsverbot (partiell)";
		}
		if(sEvintId.equals("44730a10-02bc-42bc-a7a3-ab92cff49348")){
			return "Beschäftigungsverbot (voll)";
		}
		if(sEvintId.equals("2a65b361-9eb6-4fcd-991a-ef720afd356b")){
			return "Betriebsruhe";
		}
		if(sEvintId.equals("92da2f0d-6393-43e1-a625-0e7bb63385f4")){
			return "Freistellung unwiderruflich";}
		if(sEvintId.equals("b9546c8e-2cc4-4627-9ffa-4a34081ea15c")){
			return "Bildungsurlaub";
		}
		if(sEvintId.equals("f3783feb-cf2e-4c36-afcc-b681d4092ff1")){
			return "Dienstreise";
		}
		if(sEvintId.equals("0295b765-4124-4c0a-9635-4b46735ee958")){
			return "Elternzeit";
		}
		if(sEvintId.equals("bd0b3a39-2e61-4d0e-be61-9f30dbb406cc")){
			return "Sonderurlaub";
		}
		if(sEvintId.equals("f04815ae-73f5-495e-bf21-ddae6c53dbb5")){
			return "Krankheit";
		}
		if(sEvintId.equals("8224985f-726c-4d2b-8ffc-f89e82d854f8")){
			return "Krank 1 Tag";
		}
		if(sEvintId.equals("2e11d395-16b6-4c81-8278-a32f22cca5eb")){
			return "Krank erste 4 Wochen";
		}
		if(sEvintId.equals("f7e7d602-e230-401b-b25a-4cc7867a31dc")){
			return "Krank länger als 6 Wochen";
		}
		if(sEvintId.equals("11150dfe-7a39-4f15-8ea5-290c5d43461a")){
			return "Krankheit Kind";
		}
		if(sEvintId.equals("df2d60e8-79e6-4e2d-8bdf-60b849bea479")){
			return "Krank ohne AU";
		}
		if(sEvintId.equals("fc988649-db87-43c0-a773-12a0c3e74254")){
			return "Arbeitsunfall";
		}
		if(sEvintId.equals("afe03fbb-b2f2-4ce2-9488-ed1edc7f2436")){
			return "Mutterschutz";
		}
		if(sEvintId.equals("b0c5bf2c-242c-4e1b-a9c6-6bf22dbfc97d")){
			return "Unbezahlter Urlaub";
		}
		if(sEvintId.equals("aafdf679-85a4-4450-a32e-1d9ac8b939ae")){
			return "Unentschuldigtes Fehlen";
		}
		if(sEvintId.equals("03ec5dd2-eeb8-413f-9b73-e892a36c4fff")){
			return "Urlaub";
		}
	}

	public String getAbsTypeEvintId(String sSfId) {
		if(sSfId.equals("Ausgleichstag")){
			return "7747b6ed-5991-4756-9d74-a0154431a4bf";
		}
		if(sSfId.equals("Beschäftigungsverbot (partiell)")){
			return "1a9d711b-0807-44f6-b307-74df19e6ef11";
		}
		if(sSfId.equals("Beschäftigungsverbot (voll)")){
			return "44730a10-02bc-42bc-a7a3-ab92cff49348";
		}
		if(sSfId.equals("Betriebsruhe")){
			return "2a65b361-9eb6-4fcd-991a-ef720afd356b";
		}
		if(sSfId.equals("Freistellung unwiderruflich")){
			return "92da2f0d-6393-43e1-a625-0e7bb63385f4";
		}
		if(sSfId.equals("Bildungsurlaub")){
			return "b9546c8e-2cc4-4627-9ffa-4a34081ea15c";
		}
		if(sSfId.equals("Dienstreise")){
			return "f3783feb-cf2e-4c36-afcc-b681d4092ff1";
		}
		if(sSfId.equals("Elternzeit")){
			return "0295b765-4124-4c0a-9635-4b46735ee958";
		}
		if(sSfId.equals("Sonderurlaub")){
			return "bd0b3a39-2e61-4d0e-be61-9f30dbb406cc";
		}
		if(sSfId.equals("Krankheit")){
			return "f04815ae-73f5-495e-bf21-ddae6c53dbb5";
		}
		if(sSfId.equals("Krank 1 Tag")){
			return "8224985f-726c-4d2b-8ffc-f89e82d854f8";
		}
		if(sSfId.equals("Krank erste 4 Wochen")){
			return "2e11d395-16b6-4c81-8278-a32f22cca5eb";
		}
		if(sSfId.equals("Krank länger als 6 Wochen")){
			return "f7e7d602-e230-401b-b25a-4cc7867a31dc";
		}
		if(sSfId.equals("Krankheit Kind")){
			return "11150dfe-7a39-4f15-8ea5-290c5d43461a";
		}
		if(sSfId.equals("Krank ohne AU")){
			return "df2d60e8-79e6-4e2d-8bdf-60b849bea479";
		}
		if(sSfId.equals("Arbeitsunfall")){
			return "fc988649-db87-43c0-a773-12a0c3e74254";
		}
		if(sSfId.equals("Mutterschutz")){
			return "afe03fbb-b2f2-4ce2-9488-ed1edc7f2436";
		}
		if(sSfId.equals("Unbezahlter Urlaub")){
			return "b0c5bf2c-242c-4e1b-a9c6-6bf22dbfc97d";
		}
		if(sSfId.equals("Unentschuldigtes Fehlen")){
			return "aafdf679-85a4-4450-a32e-1d9ac8b939ae";
		}
		if(sSfId.equals("Urlaub")){
			return "03ec5dd2-eeb8-413f-9b73-e892a36c4fff";
		}
		return null;
	}

	public Object deleteDupplicates(aAbsenceTypesEvint) {
		def aIds = [];
		def aAbsenceTypesEvintReturn = [];
		for(def i = 0; i<aAbsenceTypesEvint.size(); i++) {
			def bIsExisting = false;
			for( def j = 0; j< aIds.size();j++){
				if(aIds[j].equals(aAbsenceTypesEvint[i]["id"])) {
					bIsExisting = true;
				}
			}

			if(!bIsExisting) {
				aAbsenceTypesEvintReturn.push(aAbsenceTypesEvint[i]);
				aIds.push(aAbsenceTypesEvint[i]["id"]);
			}

		}
		return aAbsenceTypesEvintReturn;
	}


	public String getNameFromId(sId, aAbsenceTypes) {
		for(def i = 0; i<aAbsenceTypes.size();i++) {
			def oAbsenceType = aAbsenceTypes[i];
			if(oAbsenceType["id"].equals(sId)) {
				return 	oAbsenceType["attributes"]["name"]
			}
		}
		return null;
	}


	public boolean isDigitZero(Double dDouble) {

		int iDouble = dDouble;
		dDouble = dDouble - iDouble;
		if(dDouble == 0.0){
			return true;
		} else {
			return false
		}
	}

	public String getYear(String date){
		return (new Date(Long.valueOf(date.replace("/Date(","").replace(")/",""))).getYear()+1900)+"";
	}
	public Date getDate(String date) {
		return new Date(Long.valueOf(date.replace("/Date(","").replace(")/","")));
	}


	public Object getList(String request, jsonSlurper) {
		def firstFifty = null,
		urlAppend = null,
		counter = null,
		list = null;
		if(request.contains("evint")){
			urlAppend = "?page[offset]=";
			if (request.contains("?")) {
				urlAppend = "&page[offset]=";
			}
			firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request)).data;
			list = firstFifty;
			counter = 50;
			while (firstFifty.size() == 50) {
				firstFifty = jsonSlurper.parseText(this.httpGetFromEvint(request + urlAppend + counter)).data;
				list.addAll(firstFifty);
				counter = counter + 50;
			}
		} else{

			if(request.contains("?")){
				urlAppend = "&%24skip=";
			} else{
				urlAppend = "?%24skip=";
			}

			firstFifty = jsonSlurper.parseText(this.httpGetFromSf(request)).d.results;
			list = firstFifty;
			counter = 1000;
			while (firstFifty.size() == 1000) {
				firstFifty = jsonSlurper.parseText(this.httpGetFromSf(request + urlAppend + counter)).d.results;
				list.addAll(firstFifty);
				counter = counter + 1000;
			}
		}
		//Evint API only return 50 Entry and SF only 1000


		return list;
	}

	public String httpGetFromSf(String urlString) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic VENPTkFETUlOXzAwMUBob21lc2VUMTpUQ09OQ0hkZTIwMjEh");
		urlConnection.setRequestProperty("Accept", "application/json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			throw new Exception("\n\t\tRequest:\n\t\tGET " + urlString+"\n\t\tMessage from Request:\n\t\t" + urlConnection.getErrorStream().getText() +"\n" );
		}
	}



	public String httpGetFromEvint(String urlString) {
		def urlConnection = new URL(urlString).openConnection();
		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = inputStreamReader.read(charArray)) > 0) {
				sb.append(charArray, 0, numCharsRead);
			}

			return sb.toString();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpGetFromEvint(urlString);
			} else{
				throw new Exception("\n\t\tRequest:\n\t\tGET " + urlString+"\n\t\tMessage from Request:\n\t\t" + urlConnection.getErrorStream().getText() +"\n");
			}

		}
	}


	public String httpPatch(String urlString, String json, messageLog, String type) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		if (type.equals("PATCH")) {
			urlConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
		}
		urlConnection.setRequestMethod("POST");
		urlConnection.setDoOutput(true);
		urlConnection.getOutputStream().write(json.getBytes("UTF-8"));
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpPatch(urlString, json, messageLog, type);
			} else{
				throw new Exception("\n\t\tRequest:\n\t\t" + type + " " + urlString + "\n\t\t" + json+"\n\t\tMessage from Request:\n\t\t" + urlConnection.getErrorStream().getText()+"\n");
			}

		}
	}
	
	public String httpDelete(String urlString, messageLog) {
		def urlConnection = new URL(urlString).openConnection();

		urlConnection.setRequestProperty("Authorization", "Basic a2FqZXRhbi5ob2VibGVyQHRlYW0tY29uLmRlOnAlZjRQM3MzWFA=");
		urlConnection.setRequestProperty("Accept", "application/vnd.api+json");
		urlConnection.setRequestProperty("Content-Type", "application/vnd.api+json");
		urlConnection.setRequestMethod("DELETE");
		InputStream inputStream = null;
		try {
			inputStream = urlConnection.getInputStream();
			return inputStream.getText();
		} catch(Exception ex) {
			if(ex.toString().contains("429")){
				this.httpDelete(urlString, messageLog);
			} else{
				throw new Exception("\n\t\tRequest:\n\t\tDELETE: " + urlString +"\n\t\tMessage from Request:\n\t\t" + urlConnection.getErrorStream().getText()+"\n");
			}

		}
	}
	public Object getCorrectObject(Object object) {
		if (object instanceof java.util.ArrayList) {
			for (def i = 0; i < object.size(); i++) {
				def oEntity = object[i];
				def start = new Date().parse("yyyy-MM-dd", oEntity.start_date);
				def end = null;
				//if enddate not filled --> 9999-12-12
				if (oEntity.end_date) {
					end = new Date().parse("yyyy-MM-dd", oEntity.end_date);
				} else {
					end = new Date().parse("yyyy-MM-dd", "9999-12-31");
				}
				def now = new Date();
				def dayNow = new Date(now.getYear(), now.getMonth(), now.getDate());
				if (start.getTime() <= dayNow.getTime() && end.getTime() >= dayNow.getTime()) {
					return oEntity;
				}
			}
		} else {
			return object;
		}

	}



}
